# TrustCenter

Workshop zur Implementierung eines TrustCenters auf Basis der Blockchain im Rahmen des [DevCamp Hamburg 2018](https://hamburg.dev-camp.com/).

Die Demo Implementiert folgenden Workflow:

![workflow](workflow.png)

1. Person schickt Signatur seiner Identität in die Blockchain.
2. TrustCenter bestätigt Signatur als valide (alternativ kann auch abgelehnt werden).
3. Person eröffnet Konto bei der Bank. Dabei wird die Identität und der Public-Key übermittelt.
4. Bank prüft, ob eine passende *verifizierte* Signatur in der Blockchain ist. Falls ja, wird das Konto eröffnet. 

## Technologie-Stack

* [Spring Boot](http://spring.io/projects/spring-boot) 2 als Serverframework
* [Retrofit](https://square.github.io/retrofit/) als HTTP-Client zum lesen/schreiben auf der Blockchain
* [Vaadin](https://vaadin.com/) 10 als UI-Framework
* [jABCI](https://github.com/jTendermint/jabci) 0.24/0.26 als Schnittstelle zum Transaktionszyklus von [Tendermint](https://www.tendermint.com/) (Wichtig für Validatoren!)
* [Tendermint](https://www.tendermint.com/) als getrennt betriebene Node


## Entwicklungsvoraussetzungen

* [JDK 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Java IDE (vorzugsweise [IntelliJ](https://www.jetbrains.com/idea/) oder [Eclipse](https://www.eclipse.org/))
* Tendermint Binary
    - Für Windows v0.26.1 (bereits im Repository enthalten)
    - Für [OS X / macOS](https://github.com/tendermint/tendermint/releases/download/v0.24.0/tendermint_0.24.0_darwin_amd64.zip) oder [Linux](https://github.com/tendermint/tendermint/releases/download/v0.24.0/tendermint_0.24.0_linux_amd64.zip) muss das Binary v0.24.0 geladen werden und 'src/main/resources/tendermint' ersetzen.  
      Die jABCI Dependency muss entsprechend in der 'pom.xml' auf 0.24 gesetzt werden.

