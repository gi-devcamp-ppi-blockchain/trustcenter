package de.ppi.demo.model;

import de.ppi.demo.BeanUtil;
import de.ppi.demo.tendermint.TendermintCRUService;
import de.ppi.demo.tendermint.model.TransaktionUebermittelnAntwort;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.context.ApplicationContext;
import retrofit2.Call;
import retrofit2.Response;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Signature;
import java.time.LocalDate;
import java.util.Base64;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class IdentitaetTest {

    @Test
    void aufBlockchainSchreiben() throws Exception {

        // arrange
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        KeyPair eigenesSchluesselpaar = keyGen.generateKeyPair();

        ApplicationContext applicationContext = mock(ApplicationContext.class);
        TendermintCRUService tendermintCRUService = mock(TendermintCRUService.class);

        BeanUtil beanUtil = new BeanUtil();
        beanUtil.setApplicationContext(applicationContext);

        when(applicationContext.getBeansOfType(eq(KeyPair.class)))
                .thenReturn(Collections.singletonMap("", eigenesSchluesselpaar));
        when(applicationContext.getBeansOfType(eq(TendermintCRUService.class)))
                .thenReturn(Collections.singletonMap("", tendermintCRUService));

        Response response = Response.success(mock(TransaktionUebermittelnAntwort.class));

        Call call = mock(Call.class);
        when(call.execute()).thenReturn(response);

        ArgumentCaptor<String> schluesselWertPaar = ArgumentCaptor.forClass(String.class);
        when(tendermintCRUService.create(schluesselWertPaar.capture())).thenReturn(call);

        Identitaet identitaet = new Identitaet();
        identitaet.setAnrede(Anrede.HERR);
        identitaet.setVorname("Max");
        identitaet.setNachname("Mustermann");
        identitaet.setGeburtsdatum(LocalDate.now().minusYears(25));
        identitaet.setAusweisnummer("A00A");

        // act
        identitaet.aufBlockchainSchreiben();

        // assert
        String signatur = schluesselWertPaar.getValue().substring(38, schluesselWertPaar.getValue().length() - 1);

        Signature signierer = Signature.getInstance("SHA1WithRSA");
        signierer.initVerify(eigenesSchluesselpaar.getPublic());
        signierer.update(SerializationUtils.serialize(identitaet));
        boolean signaturKorrekt = signierer.verify(Base64.getDecoder().decode(signatur));
        assertTrue(signaturKorrekt);
    }
}