package de.ppi.demo.model;

import de.ppi.demo.BeanUtil;
import de.ppi.demo.tendermint.TendermintCRUService;
import de.ppi.demo.tendermint.model.TransaktionUebermittelnAntwort;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.context.ApplicationContext;
import retrofit2.Call;
import retrofit2.Response;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Signature;
import java.util.Base64;
import java.util.Collections;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class IdentitaetsReferenzTest {

    private TendermintCRUService tendermintCRUService;
    private KeyPair eigenesSchluesselpaar;

    @BeforeEach
    void setUp() throws Exception {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        eigenesSchluesselpaar = keyGen.generateKeyPair();

        ApplicationContext applicationContext = mock(ApplicationContext.class);
        tendermintCRUService = mock(TendermintCRUService.class);

        when(applicationContext.getBeansOfType(eq(KeyPair.class)))
                .thenReturn(Collections.singletonMap("", eigenesSchluesselpaar));
        when(applicationContext.getBeansOfType(eq(TendermintCRUService.class)))
                .thenReturn(Collections.singletonMap("", tendermintCRUService));

        BeanUtil beanUtil = new BeanUtil();
        beanUtil.setApplicationContext(applicationContext);
    }

    @Test
    void testIdentitaetBestaetigen() throws Exception {

        // arrange
        Response response = Response.success(mock(TransaktionUebermittelnAntwort.class));

        Call call = mock(Call.class);
        when(call.execute()).thenReturn(response);

        ArgumentCaptor<String> schluesselWertPaar = ArgumentCaptor.forClass(String.class);
        when(tendermintCRUService.create(schluesselWertPaar.capture())).thenReturn(call);

        IdentitaetsReferenz referenz = new IdentitaetsReferenz();

        Signature signierer = Signature.getInstance("SHA1WithRSA");
        signierer.initSign(eigenesSchluesselpaar.getPrivate());
        signierer.update(SerializationUtils.serialize("Test"));

        byte[] signature = signierer.sign();
        referenz.setSignaturIdentitaet(signature);
        referenz.setUuid(UUID.randomUUID().toString());

        // act
        referenz.bestaetigen();

        // assert
        String bestaetigung = schluesselWertPaar.getValue().substring(38, schluesselWertPaar.getValue().length() - 1);

        signierer.initVerify(eigenesSchluesselpaar.getPublic());
        signierer.update(signature);

        Assertions.assertTrue(signierer.verify(Base64.getDecoder().decode(bestaetigung)));
    }

    @Test
    void validiereBenutzerKeineBestaetigungsSignatur() throws Exception {

        // arrange
        IdentitaetsReferenz referenz = new IdentitaetsReferenz();

        // act
        boolean b = referenz.validiereBenutzer(null);

        // assert
        Assertions.assertFalse(b);
    }

    @Test
    void validiereBenutzer() throws Exception {

        // arrange
        IdentitaetsReferenz referenz = new IdentitaetsReferenz();
        Identitaet identitaet = new Identitaet();

        Signature signierer = Signature.getInstance("SHA1WithRSA");
        signierer.initSign(eigenesSchluesselpaar.getPrivate());
        signierer.update(SerializationUtils.serialize(identitaet));
        referenz.setSignaturIdentitaet(signierer.sign());


        signierer.update(referenz.getSignaturIdentitaet());
        referenz.setSignaturBestaetigung(signierer.sign());

        // act
        boolean b = referenz.validiereBenutzer(identitaet);

        // assert
        Assertions.assertTrue(b);
    }

    @Test
    void name() throws Exception {

        // arrange
        IdentitaetsReferenz referenz = new IdentitaetsReferenz();

        Signature signierer = Signature.getInstance("SHA1WithRSA");
        signierer.initSign(eigenesSchluesselpaar.getPrivate());
        signierer.update(SerializationUtils.serialize(new Identitaet()));
        referenz.setSignaturIdentitaet(signierer.sign());


        signierer.update(referenz.getSignaturIdentitaet());
        referenz.setSignaturBestaetigung(signierer.sign());

        // act
        boolean b = referenz.validiereBestaetigung();

        // assert
        Assertions.assertTrue(b);
    }
}