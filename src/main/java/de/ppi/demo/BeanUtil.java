package de.ppi.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service
public class BeanUtil implements ApplicationContextAware {

  private static ApplicationContext context;

  @Override
  public synchronized void setApplicationContext(final ApplicationContext applicationContext) {
    BeanUtil.context = applicationContext;
  }

  public static <T> T getBean(final Class<T> beanClass) {
    return context.getBeansOfType(beanClass).values().iterator().next();
  }

}