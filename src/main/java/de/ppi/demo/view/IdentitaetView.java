package de.ppi.demo.view;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.DateRangeValidator;
import com.vaadin.flow.data.validator.RegexpValidator;
import de.ppi.demo.model.Anrede;
import de.ppi.demo.model.Identitaet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

public class IdentitaetView extends Div {
    private static final Logger LOG = LoggerFactory.getLogger(IdentitaetView.class);

    private static final String PFLICHTFELD_MELDUNG = "Plichtfeld!";
    private static final String AUSWEISNUMMER_MELDUNG = "Ausweisnummer besteht aus 9 alphanumerischen Zeichen!";
    private static final String GEBURTSDATUM_MELDUNG = "Darf maximal 120 Jahre alt sein und muss schon geboren sein!";

    private static final String SPEICHERN_ERFOLGREICH_MELDUNG =
            "Transaktion in die Blockchain geschrieben. Bitte nutze '%s' zur Anmeldung";

    private ComboBox<Anrede> anrede;
    private TextField vorname;
    private TextField nachname;
    private DatePicker geburtsdatum;
    private TextField ausweisnummer;

    private Button speichern;
    private Button zuruecksetzen;

    public IdentitaetView() {
        this.getStyle().set("margin", "0 auto");
        this.setVisible(false);

        erstelleHtmlElemente();
        erstelleBinding();
    }

    private void erstelleBinding() {
        Binder<Identitaet> binder = new Binder<>();
        Identitaet identitaet = new Identitaet();

        binder.forField(anrede)
                .asRequired(PFLICHTFELD_MELDUNG)
                .bind(Identitaet::getAnrede, Identitaet::setAnrede);
        binder.forField(vorname)
                .asRequired(PFLICHTFELD_MELDUNG)
                .bind(Identitaet::getVorname, Identitaet::setVorname);
        binder.forField(nachname)
                .asRequired(PFLICHTFELD_MELDUNG)
                .bind(Identitaet::getNachname, Identitaet::setNachname);
        binder.forField(geburtsdatum)
                .withValidator(new DateRangeValidator(GEBURTSDATUM_MELDUNG,
                        LocalDate.now().minusYears(120), LocalDate.now()))
                .asRequired(PFLICHTFELD_MELDUNG)
                .bind(Identitaet::getGeburtsdatum, Identitaet::setGeburtsdatum);
        binder.forField(ausweisnummer)
                .withValidator(new RegexpValidator(AUSWEISNUMMER_MELDUNG, "^[a-zA-Z0-9]{9,}$"))
                .bind(Identitaet::getAusweisnummer, Identitaet::setAusweisnummer);

        speichern.addClickListener(event -> {
            if (binder.writeBeanIfValid(identitaet)) {
                try {
                    String referenz = identitaet.aufBlockchainSchreiben();

                    Notification notification = new Notification(String.format(SPEICHERN_ERFOLGREICH_MELDUNG, referenz));
                    notification.setPosition(Notification.Position.BOTTOM_CENTER);
                    notification.setDuration(15000);
                    notification.open();
                    UI.getCurrent().getPage().reload();
                } catch (Exception e) {
                    LOG.error("", e);
                    Notification notification = new Notification("Fehler: " + e.getMessage());
                    notification.setPosition(Notification.Position.BOTTOM_CENTER);
                    notification.setDuration(15000);
                    notification.open();
                }
            }
        });

        zuruecksetzen.addClickListener(event -> binder.readBean(null));
    }

    private void erstelleHtmlElemente() {

        anrede = new ComboBox<>("Anrede", Anrede.HERR, Anrede.FRAU);
        vorname = new TextField("Vorname", "Max");
        nachname = new TextField("Nachname", "Mustermann");
        geburtsdatum = new DatePicker("Geburtsadatum", LocalDate.now().minusYears(25));
        ausweisnummer = new TextField("Ausweisnummer", "A00A");

        speichern = new Button("Auf Blockchain schreiben");
        zuruecksetzen = new Button("Zurücksetzen");

        HorizontalLayout aktionen = new HorizontalLayout(zuruecksetzen, speichern);
        aktionen.getStyle().set("float", "right");

        FormLayout formular = new FormLayout(anrede, vorname, nachname, geburtsdatum, ausweisnummer);

        this.add(formular, aktionen);
    }
}
