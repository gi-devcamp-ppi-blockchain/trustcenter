package de.ppi.demo.view;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.DateRangeValidator;
import com.vaadin.flow.data.validator.RegexpValidator;
import de.ppi.demo.model.Anrede;
import de.ppi.demo.model.Identitaet;
import de.ppi.demo.model.IdentitaetsReferenz;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class BankView extends Div {
    private static final String PFLICHTFELD_MELDUNG = "Plichtfeld!";
    private static final String AUSWEISNUMMER_MELDUNG = "Ausweisnummer besteht aus 9 alphanumerischen Zeichen!";
    private static final String GEBURTSDATUM_MELDUNG = "Darf maximal 120 Jahre alt sein und muss schon geboren sein!";

    private static final String SPEICHERN_ERFOLGREICH_MELDUNG =
            "Transaktion in die Blockchain geschrieben. Bitte nutze '%s' zur Anmeldung";

    private ComboBox<IdentitaetsReferenz> identitaeten;
    private ComboBox<Anrede> anrede;
    private TextField vorname;
    private TextField nachname;
    private DatePicker geburtsdatum;
    private TextField ausweisnummer;

    private Button speichern;
    private Button zuruecksetzen;

    public BankView() {
        this.getStyle().set("margin", "0 auto");
        this.getStyle().set("max-width", "800px");
        this.setVisible(false);

        erstelleHtmlElemente();
        erstelleBinding();
    }

    private void erstelleBinding() {
        Binder<Identitaet> binder = new Binder<>();
        Identitaet identitaet = new Identitaet();

        binder.forField(anrede)
                .asRequired(PFLICHTFELD_MELDUNG)
                .bind(Identitaet::getAnrede, Identitaet::setAnrede);
        binder.forField(vorname)
                .asRequired(PFLICHTFELD_MELDUNG)
                .bind(Identitaet::getVorname, Identitaet::setVorname);
        binder.forField(nachname)
                .asRequired(PFLICHTFELD_MELDUNG)
                .bind(Identitaet::getNachname, Identitaet::setNachname);
        binder.forField(geburtsdatum)
                .withValidator(new DateRangeValidator(GEBURTSDATUM_MELDUNG,
                        LocalDate.now().minusYears(120), LocalDate.now()))
                .asRequired(PFLICHTFELD_MELDUNG)
                .bind(Identitaet::getGeburtsdatum, Identitaet::setGeburtsdatum);
        binder.forField(ausweisnummer)
                .withValidator(new RegexpValidator(AUSWEISNUMMER_MELDUNG, "^[a-zA-Z0-9]{9,}$"))
                .bind(Identitaet::getAusweisnummer, Identitaet::setAusweisnummer);

        zuruecksetzen.addClickListener(event -> binder.readBean(null));
        speichern.addClickListener(event -> {
            if (binder.writeBeanIfValid(identitaet)) {
                try {
                    if (!identitaeten.getValue().validiereBestaetigung()) {
                        Notification notification = new Notification("Identität ist nicht validiert!");
                        notification.setPosition(Notification.Position.BOTTOM_CENTER);
                        notification.setDuration(15000);
                        notification.open();
                    } else {
                        if (!identitaeten.getValue().validiereBenutzer(identitaet)) {
                            Notification notification = new Notification("Signatur passt nicht zu deinen Daten!");
                            notification.setPosition(Notification.Position.BOTTOM_CENTER);
                            notification.setDuration(15000);
                            notification.open();
                        } else {
                            Notification notification = new Notification("Bankkonto eröffnet!");
                            notification.setPosition(Notification.Position.BOTTOM_CENTER);
                            notification.setDuration(15000);
                            notification.open();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void erstelleHtmlElemente() {
        Label labelErlaerung = new Label("Auf dieser Seite kann ein Bankkonto eröffnet werden. " +
                "Dazu wird eine Identität benötigt, die vom TrustCenter verifiziert ist. " +
                "Zudem müssen die persönlichen Daten noch einmal genauso wie in der Identität erfasst werden, " +
                "damit die Bank diese mittels deiner Signatur in der Blockchain prüfen kann.");
        Div erklaerung = new Div(labelErlaerung);
        erklaerung.getStyle().set("padding", "24px");

        identitaeten = new ComboBox<>("Meine Identität", getItems());
        anrede = new ComboBox<>("Anrede", Anrede.HERR, Anrede.FRAU);
        vorname = new TextField("Vorname", "Max");
        nachname = new TextField("Nachname", "Mustermann");
        geburtsdatum = new DatePicker("Geburtsadatum", LocalDate.now().minusYears(25));
        ausweisnummer = new TextField("Ausweisnummer", "A00A");

        speichern = new Button("Bankkonto öffnen");
        zuruecksetzen = new Button("Zurücksetzen");

        HorizontalLayout aktionen = new HorizontalLayout(zuruecksetzen, speichern);
        aktionen.getStyle().set("float", "right");

        FormLayout formular = new FormLayout(identitaeten, anrede, vorname, nachname, geburtsdatum, ausweisnummer);

        this.add(erklaerung, formular, aktionen);
    }

    private List<IdentitaetsReferenz> getItems() {

        try {
            return IdentitaetsReferenz.alleIdentitaetenLesen();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
