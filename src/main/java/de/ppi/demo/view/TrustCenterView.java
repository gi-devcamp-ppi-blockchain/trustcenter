package de.ppi.demo.view;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import de.ppi.demo.model.IdentitaetsReferenz;

import java.io.IOException;
import java.util.List;

class TrustCenterView extends Div {
    private ComboBox<IdentitaetsReferenz> nichtBestaetigteIdentitaetsReferenzen;
    private Button identifizieren;


    TrustCenterView() {
        this.getStyle().set("margin", "0 auto");
        this.getStyle().set("width", "800px");
        this.setVisible(false);

        erstelleHtmlElemente();
        erstelleBinding();
    }

    private void erstelleHtmlElemente() {
        nichtBestaetigteIdentitaetsReferenzen = new ComboBox<>("Identitäts Referenzen", getItems());
        identifizieren = new Button("Identifizieren");

        HorizontalLayout aktionen = new HorizontalLayout(identifizieren);
        aktionen.getStyle().set("float", "right");

        FormLayout formular = new FormLayout(nichtBestaetigteIdentitaetsReferenzen);

        this.add(formular, aktionen);
    }

    private void erstelleBinding() {

        identifizieren.addClickListener(event -> {
            IdentitaetsReferenz identitaetsReferenz = nichtBestaetigteIdentitaetsReferenzen.getValue();
            try {
                identitaetsReferenz.bestaetigen();
                Notification notification = new Notification("Identität erfolgreich validiert!");
                notification.setPosition(Notification.Position.BOTTOM_CENTER);
                notification.setDuration(15000);
                notification.open();
                UI.getCurrent().getPage().reload();
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        });

    }

    private List<IdentitaetsReferenz> getItems() {

        try {
            return IdentitaetsReferenz.unbestaetigteIdentitaetenLesen();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
