package de.ppi.demo.view;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.Route;

@Route
public class MainView extends VerticalLayout {

    public MainView() {

        Tab identitaet = new Tab("Identität anlegen");
        Tab trustCenter = new Tab("TrustCenter UI");
        Tab bank = new Tab("Bank-Website");

        Tabs tabs = new Tabs(identitaet, trustCenter, bank);
        tabs.setWidth("100%");
        tabs.getStyle().set("padding", "0");
        tabs.setFlexGrowForEnclosedTabs(1);
        IdentitaetView identitaetView = new IdentitaetView();
        TrustCenterView trustCenterView = new TrustCenterView();
        BankView bankView = new BankView();

        tabs.addSelectedChangeListener((ComponentEventListener<Tabs.SelectedChangeEvent>) selectedChangeEvent -> {
            Tab aktuellerTab = selectedChangeEvent.getSource().getSelectedTab();
            if (aktuellerTab.equals(identitaet)) {
                identitaetView.setVisible(true);
                trustCenterView.setVisible(false);
                bankView.setVisible(false);
            } else if (aktuellerTab.equals(trustCenter)) {
                identitaetView.setVisible(false);
                trustCenterView.setVisible(true);
                bankView.setVisible(false);
            } else if (aktuellerTab.equals(bank)) {
                identitaetView.setVisible(false);
                trustCenterView.setVisible(false);
                bankView.setVisible(true);
            }
        });

        identitaet.setSelected(true);
        identitaetView.setVisible(true);

        this.add(tabs, identitaetView, trustCenterView, bankView);
    }

}
