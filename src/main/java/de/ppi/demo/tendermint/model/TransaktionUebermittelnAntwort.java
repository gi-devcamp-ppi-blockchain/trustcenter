package de.ppi.demo.tendermint.model;

import java.util.Objects;

public class TransaktionUebermittelnAntwort {

    private String id;
    private TransaktionError error;
    private TransaktionResult result;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TransaktionError getError() {
        return error;
    }

    public void setError(TransaktionError error) {
        this.error = error;
    }

    public TransaktionResult getResult() {
        return result;
    }

    public void setResult(TransaktionResult result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransaktionUebermittelnAntwort that = (TransaktionUebermittelnAntwort) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(error, that.error) &&
                Objects.equals(result, that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, error, result);
    }

    @Override
    public String toString() {
        return "TransaktionUebermittelnAntwort{" +
                "id='" + id + '\'' +
                ", error=" + error +
                ", result=" + result +
                '}';
    }
}
