package de.ppi.demo.tendermint.model;

public class TransaktionAbfrageResult {

    private TransaktionKeyValue response;

    public TransaktionAbfrageResult(TransaktionKeyValue response) {
        this.response = response;
    }

    public TransaktionKeyValue getResponse() {
        return response;
    }

    public void setResponse(TransaktionKeyValue response) {
        this.response = response;
    }
}
