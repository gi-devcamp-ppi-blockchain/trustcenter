package de.ppi.demo.tendermint.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class TransaktionResult {

    private Long height;
    private String hash;
    @SerializedName("check_tx")
    private TransaktionCheckTx checkTx;

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public TransaktionCheckTx getCheckTx() {
        return checkTx;
    }

    public void setCheckTx(TransaktionCheckTx checkTx) {
        this.checkTx = checkTx;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransaktionResult that = (TransaktionResult) o;
        return Objects.equals(height, that.height) &&
                Objects.equals(hash, that.hash) &&
                Objects.equals(checkTx, that.checkTx);
    }

    @Override
    public int hashCode() {
        return Objects.hash(height, hash, checkTx);
    }

    @Override
    public String toString() {
        return "TransaktionResult{" +
                "height=" + height +
                ", hash='" + hash + '\'' +
                ", checkTx=" + checkTx +
                '}';
    }
}
