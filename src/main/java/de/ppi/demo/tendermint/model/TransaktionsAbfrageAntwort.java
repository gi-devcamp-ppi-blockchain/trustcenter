package de.ppi.demo.tendermint.model;

public class TransaktionsAbfrageAntwort {

    private TransaktionAbfrageResult result;

    public TransaktionAbfrageResult getResult() {
        return result;
    }

    public void setResult(TransaktionAbfrageResult result) {
        this.result = result;
    }
}
