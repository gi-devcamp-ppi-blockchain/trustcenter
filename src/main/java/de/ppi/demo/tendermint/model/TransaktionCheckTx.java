package de.ppi.demo.tendermint.model;

import java.util.Objects;

public class TransaktionCheckTx {

    private Long code;
    private String log;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransaktionCheckTx that = (TransaktionCheckTx) o;
        return Objects.equals(code, that.code) &&
                Objects.equals(log, that.log);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, log);
    }

    @Override
    public String toString() {
        return "TransaktionCheckTx{" +
                "code=" + code +
                ", log='" + log + '\'' +
                '}';
    }
}
