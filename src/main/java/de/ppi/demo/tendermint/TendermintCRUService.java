package de.ppi.demo.tendermint;

import de.ppi.demo.tendermint.model.TransaktionUebermittelnAntwort;
import de.ppi.demo.tendermint.model.TransaktionsAbfrageAntwort;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TendermintCRUService {

    @GET("broadcast_tx_commit")
    Call<TransaktionUebermittelnAntwort> create(@Query(value = "tx") String keyValuePair);

    @GET("abci_query")
    Call<TransaktionsAbfrageAntwort> getTransaction(@Query(value = "data") String searchString);

    default Call<TransaktionsAbfrageAntwort> getTransactionBestaetigung(String searchString) {
        return this.getTransaction("C" + searchString);
    }

    default Call<TransaktionsAbfrageAntwort> getTransactionIdentitaet(String searchString) {
        return this.getTransaction("I" + searchString);
    }

    default Call<TransaktionsAbfrageAntwort> getAllTransactions() {
        return this.getTransaction("\"ALL\"");
    }

    default Call<TransaktionsAbfrageAntwort> getUnconfirmedTransactions() {
        return this.getTransaction("\"UNCONFIRMED\"");
    }

}
