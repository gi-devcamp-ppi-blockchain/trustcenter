package de.ppi.demo.tendermint;

import com.github.jtendermint.jabci.api.CodeType;
import com.github.jtendermint.jabci.api.ICheckTx;
import com.github.jtendermint.jabci.api.ICommit;
import com.github.jtendermint.jabci.api.IDeliverTx;
import com.github.jtendermint.jabci.api.IQuery;
import com.github.jtendermint.jabci.socket.ExceptionListener;
import com.github.jtendermint.jabci.socket.TSocket;
import com.github.jtendermint.jabci.types.KVPair;
import com.github.jtendermint.jabci.types.RequestCheckTx;
import com.github.jtendermint.jabci.types.RequestCommit;
import com.github.jtendermint.jabci.types.RequestDeliverTx;
import com.github.jtendermint.jabci.types.RequestQuery;
import com.github.jtendermint.jabci.types.ResponseCheckTx;
import com.github.jtendermint.jabci.types.ResponseCommit;
import com.github.jtendermint.jabci.types.ResponseDeliverTx;
import com.github.jtendermint.jabci.types.ResponseQuery;
import com.google.protobuf.ByteString;
import de.ppi.demo.model.IdentitaetsReferenz;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

@Service
public class KeyValueStore implements IDeliverTx, ICheckTx, ICommit, IQuery {

    private static final Logger LOG = LoggerFactory.getLogger(KeyValueStore.class);

    private final TSocket socket;

    private final Map<String, String> identeties = new HashMap<>();
    private final Map<String, String> confirmations = new HashMap<>();

    public KeyValueStore() {
        socket = new TSocket((socket, event, exception) -> {
            if (event == ExceptionListener.Event.SocketHandler_handleRequest) {
                LOG.error("", exception);
            } else if (event == ExceptionListener.Event.SocketHandler_readFromStream) {
                LOG.info("error on {} -> SocketHandler_readFromStream: {}", socket.orElse("NONAME"), exception.getMessage());
            }
        }, (socketName, count) -> {
            LOG.info("CONNECT socketname: {} count: {}", socketName, count);
        }, (socketName, count) -> {
            LOG.info("DISCONNET socketname: {} count: {}", socketName, count);
        });

        socket.registerListener(this);

        Thread t = new Thread(() -> socket.start(TSocket.DEFAULT_LISTEN_SOCKET_PORT));
        t.setName("Java Counter Main Thread");
        t.start();
    }

    @Override
    public ResponseCheckTx requestCheckTx(RequestCheckTx req) {
        LOG.info("got check tx");
        String txString = req.getTx().toString(Charset.forName("UTF8"));

        if (txString.length() < 40 || (txString.charAt(36) != 'I' && txString.charAt(36) != 'C')) {
            return ResponseCheckTx.newBuilder()
                    .setCode(CodeType.BadNonce)
                    .setLog("Transaktion hat nicht die Form UUID + I/C + SIGNATUR!").build();
        }

        return ResponseCheckTx.newBuilder().setCode(CodeType.OK).build();
    }

    @Override
    public ResponseCommit requestCommit(RequestCommit requestCommit) {
        LOG.info("requestCommit");
        return ResponseCommit.newBuilder().build();
    }

    @Override
    public ResponseDeliverTx receivedDeliverTx(RequestDeliverTx req) {
        LOG.info("got deliver tx");
        String txString = req.getTx().toString(Charset.forName("UTF8"));
        LOG.info("with {}", txString);

        if (txString.length() < 40 || (txString.charAt(36) != 'I' && txString.charAt(36) != 'C')) {
            return ResponseDeliverTx.newBuilder()
                    .setCode(CodeType.BadNonce)
                    .setLog("Transaktion hat nicht die Form UUID + I/C + SIGNATUR!").build();
        }

        String key = txString.substring(0, 36);
        String value = txString.substring(37);

        if (txString.charAt(36) == 'I') {
            this.identeties.put(key, value);
        }        
        if (txString.charAt(36) == 'C') {
            if(!identeties.containsKey(key)) {
                return ResponseDeliverTx.newBuilder()
                        .setCode(CodeType.BadNonce)
                        .setLog("Identitäts-Signatur noch nicht in der Blockchain!").build();
            }
            this.confirmations.put(key, value);
        }

        return ResponseDeliverTx.newBuilder()
                .setCode(CodeType.OK)
                .addTags(KVPair.newBuilder()
                        .setKey(ByteString.copyFromUtf8(key))
                        .setValue(ByteString.copyFromUtf8(value)))
                .build();
    }

    @Override
    public ResponseQuery requestQuery(RequestQuery req) {
        String key = req.getData().toString(Charset.forName("UTF8"));

        if (this.identeties.containsKey(key)) {
            ResponseQuery.Builder builder = ResponseQuery.newBuilder().setCode(CodeType.OK);

            IdentitaetsReferenz identitaetsReferenz = new IdentitaetsReferenz();
            identitaetsReferenz.setUuid(key);
            identitaetsReferenz.setSignaturIdentitaet(Base64.getDecoder().decode(this.identeties.get(key).getBytes()));
            identitaetsReferenz.setSignaturBestaetigung(this.confirmations.get(key) != null
                    ? Base64.getDecoder().decode(this.confirmations.get(key).getBytes())
                    : null);

            byte[] data = SerializationUtils.serialize(identitaetsReferenz);

            return builder
                    .setKey(req.getData())
                    .setValue(ByteString.copyFrom(data))
                    .build();
        }

        if ("ALL".equals(key)) {
            return ResponseQuery.newBuilder().setCode(CodeType.OK)
                    .setKey(req.getData())
                    .setValue(ByteString.copyFrom(SerializationUtils.serialize(new HashSet<>(this.identeties.keySet()))))
                    .build();
        }
        if ("UNCONFIRMED".equals(key)) {
            HashSet<String> unconfirmed = new HashSet<>(this.identeties.keySet());
            unconfirmed.removeAll(this.confirmations.keySet());
            return ResponseQuery.newBuilder().setCode(CodeType.OK)
                    .setKey(req.getData())
                    .setValue(ByteString.copyFrom(SerializationUtils.serialize(unconfirmed)))
                    .build();
        }

        return ResponseQuery.newBuilder().setCode(CodeType.BadNonce)
                .setLog("Key nicht vorhanden!")
                .build();
    }
}
