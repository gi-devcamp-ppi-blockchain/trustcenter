package de.ppi.demo.model;

import de.ppi.demo.BeanUtil;
import de.ppi.demo.tendermint.TendermintCRUService;
import de.ppi.demo.tendermint.model.TransaktionUebermittelnAntwort;
import de.ppi.demo.tendermint.model.TransaktionsAbfrageAntwort;
import org.apache.commons.lang3.SerializationUtils;
import retrofit2.Response;

import java.io.IOException;
import java.io.Serializable;
import java.security.KeyPair;
import java.security.Signature;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class IdentitaetsReferenz implements Serializable {

    private String uuid;
    private byte[] signaturIdentitaet;
    private byte[] signaturBestaetigung;

    public static List<IdentitaetsReferenz> unbestaetigteIdentitaetenLesen() throws IOException {
        TendermintCRUService cruService = BeanUtil.getBean(TendermintCRUService.class);

        TransaktionsAbfrageAntwort antwort = cruService.getUnconfirmedTransactions().execute().body();

        byte[] value = Base64.getDecoder().decode(antwort.getResult().getResponse().getValue());
        HashSet<String> uuids = SerializationUtils.deserialize(value);

        return uuids.stream().map(uuid -> leseTransaktion(cruService, uuid)).collect(Collectors.toList());
    }

    public static List<IdentitaetsReferenz> alleIdentitaetenLesen() throws IOException {
        TendermintCRUService cruService = BeanUtil.getBean(TendermintCRUService.class);

        TransaktionsAbfrageAntwort antwort = cruService.getAllTransactions().execute().body();

        byte[] value = Base64.getDecoder().decode(antwort.getResult().getResponse().getValue());
        HashSet<String> uuids = SerializationUtils.deserialize(value);

        return uuids.stream().map(uuid -> leseTransaktion(cruService, uuid)).collect(Collectors.toList());
    }

    private static IdentitaetsReferenz leseTransaktion(TendermintCRUService cruService, String uuid) {
        try {
            TransaktionsAbfrageAntwort antwort = cruService.getTransaction(String.format("\"%s\"", uuid)).execute().body();
            byte[] transaction = Base64.getDecoder().decode(antwort.getResult().getResponse().getValue());
            return SerializationUtils.deserialize(transaction);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void bestaetigen() throws Exception {
        TendermintCRUService cruService = BeanUtil.getBean(TendermintCRUService.class);

        KeyPair eigenesSchluesselpaar = BeanUtil.getBean(KeyPair.class);

        Signature signierer = Signature.getInstance("SHA1WithRSA");
        signierer.initSign(eigenesSchluesselpaar.getPrivate());
        signierer.update(this.getSignaturIdentitaet());

        String signatur = Base64.getEncoder().encodeToString(signierer.sign());

        Response<TransaktionUebermittelnAntwort> ergebnis = cruService
                .create(String.format("\"%sC%s\"", this.getUuid(), signatur)).execute();

        if (ergebnis.body().getError() != null) {
            throw new RuntimeException(ergebnis.body().getError().getData());
        } else if (ergebnis.body().getResult() != null
                && ergebnis.body().getResult().getCheckTx().getCode() != null
                && ergebnis.body().getResult().getCheckTx().getCode() == 2) {
            throw new RuntimeException(ergebnis.body().getResult().getCheckTx().getLog());
        }
    }

    public boolean validiereBenutzer(Identitaet identitaet) throws Exception {
        if (getSignaturBestaetigung() == null) {
            return false;
        }
        KeyPair eigenesSchluesselpaar = BeanUtil.getBean(KeyPair.class);

        Signature signierer = Signature.getInstance("SHA1WithRSA");
        signierer.initVerify(eigenesSchluesselpaar.getPublic());
        signierer.update(SerializationUtils.serialize(identitaet));
        return signierer.verify(getSignaturIdentitaet());
    }

    public boolean validiereBestaetigung() throws Exception {
        if (getSignaturBestaetigung() == null) {
            return false;
        }
        KeyPair eigenesSchluesselpaar = BeanUtil.getBean(KeyPair.class);

        Signature signierer = Signature.getInstance("SHA1WithRSA");
        signierer.initVerify(eigenesSchluesselpaar.getPublic());
        signierer.update(getSignaturIdentitaet());
        return signierer.verify(getSignaturBestaetigung());
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public byte[] getSignaturIdentitaet() {
        return signaturIdentitaet;
    }

    public void setSignaturIdentitaet(byte[] signaturIdentitaet) {
        this.signaturIdentitaet = signaturIdentitaet;
    }

    public byte[] getSignaturBestaetigung() {
        return signaturBestaetigung;
    }

    public void setSignaturBestaetigung(byte[] signaturBestaetigung) {
        this.signaturBestaetigung = signaturBestaetigung;
    }

    @Override
    public String toString() {
        return uuid;
    }
}
