package de.ppi.demo.model;

public enum Anrede {
    HERR,
    FRAU
}
