package de.ppi.demo.model;

import de.ppi.demo.BeanUtil;
import de.ppi.demo.tendermint.TendermintCRUService;
import de.ppi.demo.tendermint.model.TransaktionUebermittelnAntwort;
import org.apache.commons.lang3.SerializationUtils;
import retrofit2.Response;

import java.io.Serializable;
import java.security.KeyPair;
import java.security.Signature;
import java.time.LocalDate;
import java.util.Base64;
import java.util.UUID;

public class Identitaet implements Serializable {

    private Anrede anrede;
    private String vorname;
    private String nachname;
    private LocalDate geburtsdatum;
    private String ausweisnummer;

    public String aufBlockchainSchreiben() throws Exception {

        String key = UUID.randomUUID().toString();

        KeyPair eigenesSchluesselpaar = BeanUtil.getBean(KeyPair.class);

        Signature signierer = Signature.getInstance("SHA1WithRSA");
        signierer.initSign(eigenesSchluesselpaar.getPrivate());
        signierer.update(SerializationUtils.serialize(this));

        String signatur = Base64.getEncoder().encodeToString(signierer.sign());

        TendermintCRUService cruService = BeanUtil.getBean(TendermintCRUService.class);
        Response<TransaktionUebermittelnAntwort> ergebnis = cruService
                .create(String.format("\"%sI%s\"", key, signatur)).execute();

        if (ergebnis.body().getError() != null) {
            throw new RuntimeException(ergebnis.body().getError().getData());
        } else if (ergebnis.body().getResult() != null
                && ergebnis.body().getResult().getCheckTx().getCode() != null
                && ergebnis.body().getResult().getCheckTx().getCode() == 2) {
            throw new RuntimeException(ergebnis.body().getResult().getCheckTx().getLog());
        }

        return key;
    }

    public Anrede getAnrede() {
        return anrede;
    }

    public void setAnrede(Anrede anrede) {
        this.anrede = anrede;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public LocalDate getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(LocalDate geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public String getAusweisnummer() {
        return ausweisnummer;
    }

    public void setAusweisnummer(String ausweisnummer) {
        this.ausweisnummer = ausweisnummer;
    }

    @Override
    public String toString() {
        return "Identitaet{" +
                "anrede=" + anrede +
                ", vorname='" + vorname + '\'' +
                ", nachname='" + nachname + '\'' +
                ", geburtsdatum=" + geburtsdatum +
                ", ausweisnummer='" + ausweisnummer + '\'' +
                '}';
    }
}
