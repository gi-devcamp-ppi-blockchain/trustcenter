package de.ppi.demo;

import de.ppi.demo.tendermint.TendermintCRUService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

@SpringBootApplication
public class Application {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        final String tendermintPath = "./src/main/resources/tendermint";
        new Thread(() -> {
            try {
                new ProcessBuilder(tendermintPath, "init").inheritIO().start();
                new ProcessBuilder(tendermintPath, "unsafe_reset_all").inheritIO().start();
                new ProcessBuilder(tendermintPath, "node", "--consensus.create_empty_blocks=false").inheritIO().start();
            } catch (IOException e) {
                LOG.error("", e);
                throw new Error("Tendermint konnte nicht gestartet werden!");
            }
        }).start();

        SpringApplication.run(Application.class);
    }

    @Bean
    public Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://localhost:26657")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Bean
    public TendermintCRUService getCruService(Retrofit retrofit) {
        return retrofit.create(TendermintCRUService.class);
    }

    @Bean
    @Scope("singleton")
    public KeyPair getOwnKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        return keyGen.generateKeyPair();
    }
}
